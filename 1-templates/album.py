"""Python script to generate HTML files for showing a photo album.

USAGE: python album.py dirname/

Uses Jinja2 templates for creating the HTML files.
"""

import sys
import os
from jinja2 import Template

def render_template(filename, **kwargs):
    t = Template(open("templates/" + filename).read())
    return t.render(**kwargs)

def find_jpeg_files(dirname):
    return [f for f in os.listdir(dirname) if f.endswith(".jpg")]

def write_file(filename, contents):
    print "generating", filename
    f = open(filename, "w")
    f.write(contents)
    f.close()

def create_html(dirname, jpgfile, prev_jpg, next_jpg):
    prev_html = prev_jpg.replace(".jpg", ".html")
    next_html = next_jpg.replace(".jpg", ".html")

    kwargs = {"photo": jpgfile,
              "prev": prev_html, 
              "next": next_html}
    
    htmlfile = os.path.join(dirname, jpgfile.replace(".jpg", ".html"))
    write_file(htmlfile, render_template("photo.html", **kwargs))

def create_index(dirname, filenames):
    photos = [{"url": f.replace(".jpg", ".html"), "img": f} for f in filenames]
    path = os.path.join(dirname, "index.html")
    write_file(path, render_template("index.html", photos=photos))

def main():
    dirname = sys.argv[1]
    jpegs = find_jpeg_files(dirname)
    for i, f in enumerate(jpegs):
        prev = jpegs[i-1]
        # rollback to the first image if the current image
        # is the last one
        next = jpegs[(i+1) % len(jpegs)] 
        create_html(dirname, f, prev, next)
    create_index(dirname, jpegs)
        
if __name__ == "__main__":
    main()
