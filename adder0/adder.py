
from flask import Flask, request, render_template

app = Flask(__name__)

@app.route("/")
def index():
    return render_template("adder.html")

@app.route("/add")
def add():
    x = request.args.get("x")
    y = request.args.get("y")
    # TODO: error handling
    z = int(x) + int(y)
    return render_template("adder_result.html",
                           x=x, y=y, z=z)
    
    
if __name__ == "__main__":
    app.run()