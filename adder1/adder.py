
from flask import (
    Flask, flash, 
    request, redirect, session, 
    render_template, make_response)

app = Flask(__name__)
app.secret_key = "adfn;fkdkaffdf"

@app.context_processor
def template_globals():
    """This function is called before rendering 
    a template. This should return a dictionary
    and all the key-value pairs in the dict will 
    be make available to the template.
    """
    name = session.get("name")
    return {"name": name}

@app.route("/")
def index():
    return render_template("adder.html")

@app.route("/add")
def add():
    x = request.args.get("x")
    y = request.args.get("y")
    # TODO: error handling
    z = int(x) + int(y)

    # Get the current count from cookie
    # and increment it by one
    count = 1 + int(request.cookies.get("count", 0))

    contents = render_template("adder_result.html",
                           x=x, y=y, z=z, count=count)

    # make a response so that we can set a cookie    
    response = make_response(contents)
    response.set_cookie("count", str(count))
    return response

@app.route("/login", methods=['GET', 'POST'])
def login():
    if request.method == "GET":
        return render_template("login.html")
    else:
        name = request.form['name']
        session['name'] = name

        flash("Welcome " +  name + "!")

        response = redirect("/")
        return response
    
if __name__ == "__main__":
    app.run(debug=True)