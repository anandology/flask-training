from flask import Flask, render_template
app = Flask(__name__)

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/photos")
def photos():
    return render_template("photos.html")

@app.route("/upload")
def userpage():
    return render_template("upload.html")

@app.route("/photos/<int:photo_id>")
def photopage(photo_id):
    return render_template("photo.html")

if __name__ == "__main__":
    app.run()