import os
from flask import (
    Flask, render_template,
    request, url_for, 
    redirect, flash, send_file)

# Import SQLALchemy flask extension
from flask.ext.sqlalchemy import SQLAlchemy

app = Flask(__name__, static_url_path='')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///clickr.db'
app.config['SQLALCHEMY_ECHO'] = True
app.secret_key = "change-this-on-production"

# create the database object
db = SQLAlchemy(app)

class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(30))

    def __repr__(self):
        return "Photo(id={0})".format(self.id)

def get_photo(id):
    return Photo.query.filter_by(id=id).first()

def get_all_photos():
    return Photo.query.all()

def save_photo(title, photo_file):
    title = request.form['title']
    photo = Photo(title=title)
    db.session.add(photo)
    db.session.commit()
    path = os.path.join(PHOTO_ROOT, "{0}.jpg".format(photo.id))
    photo_file.save(path)
    return photo

PHOTO_ROOT = "photos"

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/photos")
def photos():
    return render_template("photos.html", photos=get_all_photos())

@app.route("/upload", methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        title = request.form['title']
        photo_file = request.files['photo']
        photo = save_photo(title, photo_file)
        flash("Thank you for uploading!")
        return redirect(url_for("photopage", photo_id=photo.id))
    else:
        return render_template("upload.html")

@app.route("/photos/<int:photo_id>")
def photopage(photo_id):
    photo = get_photo(photo_id)
    if photo:
        return render_template("photo.html", photo=photo)
    else:
        abort(404)

@app.route("/photos/<int:photo_id>.jpg")
def photo_image(photo_id):
    photo = get_photo(photo_id)
    if photo:
        path = "{0}/{1}.jpg".format(PHOTO_ROOT, photo_id)
        return send_file(path)
    else:
        abort(404)

if __name__ == "__main__":
    app.run(debug=True)