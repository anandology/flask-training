import os
from flask import (
    Flask, render_template,
    request, url_for, session,
    redirect, flash, send_file)

app = Flask(__name__, static_url_path='')
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///clickr2.db'
app.config['SQLALCHEMY_ECHO'] = True
app.secret_key = "change-this-on-production"

import models

@app.context_processor
def template_globals():
    """This function is called before rendering
    a template. This should return a dictionary
    and all the key-value pairs in the dict will
    be make available to the template.
    """
    username = session.get("username")
    return {"username": username}

def save_photo(title, photo_file):
    title = request.form['title']
    photo = Photo(title=title)
    db.session.add(photo)
    db.session.commit()
    path = os.path.join(PHOTO_ROOT, "{0}.jpg".format(photo.id))
    photo_file.save(path)
    return photo

PHOTO_ROOT = "photos"

@app.route("/")
def index():
    return render_template("index.html")

@app.route("/photos")
def photos():
    return render_template("photos.html", photos=models.get_all_photos())

@app.route("/upload", methods=['GET', 'POST'])
def upload():
    if request.method == 'POST':
        title = request.form['title']
        photo_file = request.files['photo']
        photo = save_photo(title, photo_file)
        flash("Thank you for uploading!")
        return redirect(url_for("photopage", photo_id=photo.id))
    else:
        return render_template("upload.html")

@app.route("/photos/<int:photo_id>")
def photopage(photo_id):
    photo = models.get_photo(photo_id)
    if photo:
        return render_template("photo.html", photo=photo)
    else:
        abort(404)

@app.route("/photos/<int:photo_id>.jpg")
def photo_image(photo_id):
    photo = models.get_photo(photo_id)
    if photo:
        path = "{0}/{1}.jpg".format(PHOTO_ROOT, photo_id)
        return send_file(path)
    else:
        abort(404)

@app.route('/signup',methods=['GET', 'POST'])
def signup():
    if request.method == "POST":
        name = request.form['name']
        user = models.User.query.filter_by(name=name).first()
        if user:
            return render_template("signup.html", user=user)  
        else:
            user = models.User(name=name)
            models.db.session.add(user)
            models.db.session.commit()
            return redirect("/")
    else:
        return render_template("signup.html", user=None)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if request.method == "POST":
        name = request.form['name']
        user = models.User.query.filter_by(name=name).first()
        if user:
            session['username'] = user.name
            return redirect("/")
        else:
            return render_template("login.html", name=name)
    else:
        return render_template("login.html")

if __name__ == "__main__":
    models.db.create_all()
    app.run(debug=True)