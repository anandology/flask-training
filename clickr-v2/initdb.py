from clickr import db, Photo

# creates all the tables if not already created.
db.create_all()

# Insert a new photo in the database
p = Photo(title="First photo")
db.session.add(p)
db.session.commit()

# query the database for all photos
print Photo.query.all()

# query photo by id
print Photo.query.filter_by(id=1).first()