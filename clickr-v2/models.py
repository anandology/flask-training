# Import SQLALchemy flask extension
from flask.ext.sqlalchemy import SQLAlchemy

from clickr import app

# create the database object
db = SQLAlchemy(app)

class Photo(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(30))
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return "Photo(id={0})".format(self.id)

class User(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(30))
    email = db.Column(db.String(30))
    password = db.Column(db.String(30))

    photos = db.relationship('Photo', backref='owner', lazy='dynamic')

def get_photo(id):
    return Photo.query.filter_by(id=id).first()

def get_all_photos():
    return Photo.query.all()
